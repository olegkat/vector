package vector

import (
	"testing"
)

type Int int

func (d Int) Cmp(c interface{}) bool {
	t := c.(Int)
	return t == d
}

func (d Int) Scale(f interface{}) interface{} {
	t := f.(Int)
	return Int(d * t)
}

func (d Int) Add(s interface{}) interface{} {
	t := s.(Int)
	return Int(d + t)
}

func (d Int) Subtract(s interface{}) interface{} {
	t := s.(Int)
	return Int(d - t)
}

func (d Int) Multiply(p interface{}) interface{} {
	t := p.(Int)
	return Int(d * t)
}

func TestVector(t *testing.T) {
	v := Vector{Int(5), Int(2), Int(7)}

	if !v.Cmp(&Vector{Int(5), Int(2), Int(7)}) {
		t.Error()
	}

	if v.Cmp(&Vector{Int(6), Int(2), Int(7)}) {
		t.Error()
	}

	v.Scale(Int(3))
	if !v.Cmp(&Vector{Int(15), Int(6), Int(21)}) {
		t.Error()
	}

	v.Add(&Vector{Int(1), Int(0), Int(3)})
	if !v.Cmp(&Vector{Int(16), Int(6), Int(24)}) {
		t.Error()
	}

	w := Vector{Int(3), Int(4), Int(1)}
	dp := v.DotProd(&w)
	if dp != Int(96) {
		t.Error()
	}

	n := Vector{}
	np := n.DotProd(&Vector{})
	if np != nil {
		t.Error()
	}

	w.CrossProd(&Vector{Int(2), Int(1), Int(-2)})
	if !w.Cmp(&Vector{Int(-9), Int(8), Int(-5)}) {
		t.Error()
	}

	ls := w.LenSq()
	if ls != Int(81+64+25) {
		t.Error()
	}
}
