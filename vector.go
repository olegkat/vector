// Package vector implements vectors of arbitrary component types, and
// various operations on vectors.
package vector

import "fmt"

type Vector []interface{}

// Vector operations need to perform some operations on vector
// components.  Component types should implement one or more of the
// following interfaces, depending on which operations are to be
// performed on vectors.  See documentation of an operation for the
// interfaces required by the operation.
type Comparable interface {
	Cmp(interface{}) bool
}

type Scalable interface {
	Scale(interface{}) interface{}
}

type Addable interface {
	Add(interface{}) interface{}
}

type Subtractable interface {
	Subtract(interface{}) interface{}
}

type Multipliable interface {
	Multiply(interface{}) interface{}
}

// Scale multiplies the vector by a scalar.  Vector components must
// implement Scalable.
func (v *Vector) Scale(f interface{}) {
	for i, _ := range *v {
		t := (*v)[i].(Scalable)
		(*v)[i] = t.Scale(f)
	}
}

// Cmp compares a vector to another vector.  Vector components must
// implement Comparable.  Vectors must have equal dimensions.
func (v *Vector) Cmp(w *Vector) bool {
	assertEqDim(v, w)

	for i, _ := range *v {
		t := (*v)[i].(Comparable)
		if !t.Cmp((*w)[i]) {
			return false
		}
	}

	return true
}

// Add adds another vector to the vector.  Vector components must
// implement Addable.  Vectors must have equal dimensions.
func (v *Vector) Add(w *Vector) {
	assertEqDim(v, w)

	for i, _ := range *v {
		t := (*v)[i].(Addable)
		(*v)[i] = t.Add((*w)[i])
	}
}

// DotProd returns the dot product between the vector and another
// vector.  Vector components must implement Multipliable and Addable.
// Vectors must have equal dimensions.  DotProd on zero length vectors
// returns nil.
func (v *Vector) DotProd(w *Vector) interface{} {
	assertEqDim(v, w)

	if len(*v) == 0 {
		return nil
	}

	t := (*v)[0].(Multipliable)
	dp := t.Multiply((*w)[0]).(Addable)

	for i := 1; i < len(*v); i++ {
		t := (*v)[i].(Multipliable)
		dp = dp.Add(t.Multiply((*w)[i])).(Addable)
	}

	return dp
}

// LenSq returns the square of the vector length.  Vector components
// must satisfy requirments of DotProd.
func (v *Vector) LenSq() interface{} {
	return (*v).DotProd(v)
}

// CrossProd calculates the cross product between the vector and
// another vector.  Vector components must implement Multipliable and
// Subtractable.  Vectors must be three-dimensional vectors.
func (v *Vector) CrossProd(w *Vector) {
	assertEqDim(v, w)

	if len(*v) != 3 {
		panic("Cross product is only supported for three-dimensional vectors.")
	}

	v0 := (*v)[0].(Multipliable)
	v1 := (*v)[1].(Multipliable)
	v2 := (*v)[2].(Multipliable)

	v1w2 := v1.Multiply((*w)[2]).(Subtractable)
	v2w1 := v2.Multiply((*w)[1])

	v2w0 := v2.Multiply((*w)[0]).(Subtractable)
	v0w2 := v0.Multiply((*w)[2])

	v0w1 := v0.Multiply((*w)[1]).(Subtractable)
	v1w0 := v1.Multiply((*w)[0])

	(*v)[0] = v1w2.Subtract(v2w1)
	(*v)[1] = v2w0.Subtract(v0w2)
	(*v)[2] = v0w1.Subtract(v1w0)
}

func assertEqDim(v, w *Vector) {
	if len(*v) != len(*w) {
		panic(fmt.Sprintf(
			"Vectors must have equal dimensions (%d != %d).",
			len(*v),
			len(*w)))
	}
}
